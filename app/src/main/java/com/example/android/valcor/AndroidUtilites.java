package com.example.android.valcor;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Created by or185014 on 3/8/2016.
 * This class is just used to hold some useful functions that can possibly be reused elsewhere
 * This class right now is not actually being used
 */
public class AndroidUtilites {

    //Hide The keyboard usually after hitting enter
    //or submit when filling out a text box call this method
    public void HideSoftKeyboard(View view, InputMethodManager imm) {
        //View view = this.getCurrentFocus();
        if (view != null) {
            //InputMethodManager imm = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    //If you having a lot of things showing toast at similar times
    //and just want to show the last thing that called toast
    //use this to create toast
    private void DisplayToast(String msg, Toast mToast, Context context) {
        if (mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
