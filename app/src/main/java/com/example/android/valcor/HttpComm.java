package com.example.android.valcor;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by or185014 on 4/28/2016.
 */
public class HttpComm implements IHttpCommunicator {
    private String basePath = "";
    private String route = "";
    private String requestString = "";
    private Context mContext;
    private HttpResponseListener mListener;
    private int mRequestMethod = Request.Method.GET;
    private String mJsonBody = "{}";

    public HttpComm(Context context, Activity activity){
        mContext = context;
        try {
            mListener = (HttpResponseListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement HttpResponseListener");
        }
    }

    @Override
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public void setRoute(String route){
        //TODO: make sure slashes are added correctly
        this.route = route;
    }

    public void setRequestMethod(int method){
        mRequestMethod = method;
    }

    public void setJsonBody(String jsonBody){
        this.mJsonBody = jsonBody;
    }

    public void stringRequest(){
        // Instantiate the RequestQueue.
        RequestQueue queue = RequestQueueSingleton.getInstance(mContext).getRequestQueue();
        requestString = basePath + route;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(mRequestMethod, requestString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Response:", response);
                        mListener.onSuccessfulResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Response:", error.getMessage());
                mListener.onErrorResponse(error.getMessage());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void jsonRequest(){
        // Instantiate the RequestQueue.
        RequestQueue queue = RequestQueueSingleton.getInstance(mContext).getRequestQueue();
        requestString = basePath + route;
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject(mJsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Request a string response from the provided URL.
        JsonObjectRequest stringRequest = new JsonObjectRequest(mRequestMethod, requestString, jsonBody ,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        Log.d("Response:", response.toString());
                        mListener.onSuccessfulResponse(response.toString());
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Response:", error.getMessage());
                    mListener.onErrorResponse(error.getMessage());
                }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public interface  HttpResponseListener{
        public void onSuccessfulResponse(String res);
        public void onErrorResponse(String error);
    }
}
