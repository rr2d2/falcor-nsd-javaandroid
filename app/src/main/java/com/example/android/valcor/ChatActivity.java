/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.valcor;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;

public class ChatActivity extends Activity implements BoardFragment.Communicator, ChatFragment.GameCommunicator, HttpComm.HttpResponseListener {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ChatFragment chatFragment;
    BoardFragment boardFragment;
    UserInfoFragment userInfoFragment;

    private NsdHelper mNsdHelper;
    private ChatConnection mConnection;
    private Toast mToast;
    private HttpComm httpComm;
    private String mId="2389";

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        SetupFragments();

       /* httpComm = new HttpComm(getApplicationContext(), this);
        httpComm.setBasePath("http://52.37.97.113:9000");
        httpComm.setRoute("/api/things/"+mId);
        httpComm.setRequestMethod(Request.Method.GET);
        httpComm.stringRequest();*/
    }

    private void SetupFragments() {
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        chatFragment = new ChatFragment();
        boardFragment = new BoardFragment();
        userInfoFragment = new UserInfoFragment();

        fragmentTransaction.add(R.id.main, chatFragment, "chatfragment");
        fragmentTransaction.add(R.id.main, boardFragment, "boardfragment");
        fragmentTransaction.add(R.id.main, userInfoFragment, "userfragment");

        fragmentTransaction.hide(boardFragment);
        fragmentTransaction.hide(chatFragment);

        fragmentTransaction.commit();
    }

    public void start(View v){
        HideSoftKeyboard();

        String userName = ((EditText)findViewById(R.id.userNameTxt)).getText().toString();
        if (!userName.isEmpty()){
            DisplayToast("Hello "+userName);
            mConnection.userName = userName;
            chatFragment.userName = userName;
            boardFragment.UpdateUserName(userName);
        }
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(userInfoFragment);
        fragmentTransaction.show(chatFragment);
        fragmentTransaction.commit();
    }

    private void HideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void LaunchGame(View v){
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.hide(chatFragment);
        fragmentTransaction.show(boardFragment);
        fragmentTransaction.commit();

        boardFragment.setCommunicator(this);
        chatFragment.SetGameCommunicator(this);

        boardFragment.UpdateUserName(chatFragment.userName);
        boardFragment.UpdateOpponentName(chatFragment.opponentsName);
    }

    private void DisplayToast(String msg) {
        if (mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent keyEvent){

        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            fragmentTransaction = fragmentManager.beginTransaction();

            if (boardFragment != null && boardFragment.isVisible()) {
                fragmentTransaction.hide(boardFragment);
                fragmentTransaction.show(chatFragment);
                fragmentTransaction.commit();
            }
            else if (chatFragment != null && chatFragment.isVisible()){
                fragmentTransaction.hide(chatFragment);
                fragmentTransaction.show(userInfoFragment);
                fragmentTransaction.commit();
            }
            return false;
        }
        return super.onKeyDown(keyCode, keyEvent);
    }

    @Override
    protected void onPause() {
        if (mNsdHelper != null) {
            mNsdHelper.tearDown();
        }

        super.onPause();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if (chatFragment.connected){
            return;
        }
        mNsdHelper = chatFragment.mNsdHelper;
        mConnection = chatFragment.mConnection;

        if (mNsdHelper != null) {
            mNsdHelper.discoverServices();
        }
    }
    
    @Override
    protected void onDestroy() {
         if (mNsdHelper != null){
            mNsdHelper.tearDown();
        }
        if (mConnection != null){
            mConnection.tearDown();
        }
        if (chatFragment != null){
            chatFragment.connected = false;
        }
        super.onDestroy();
    }

    //This method will be used for communication between boardFragment and the chatFragment
    @Override
    public void respond(String data) {
        DisplayToast(data);
        if (chatFragment.connected){
            if (data.toLowerCase() == "winner"){
                //also need to send message to display move on other side
                mConnection.sendMessage(data);
            } else if(data.toLowerCase() == "tie"){
                //also need to send message to display move on other side
                mConnection.sendMessage(data);
            } else {
                //also need to send message to display move on other side
                mConnection.sendMessage(data);
                mConnection.sendMove(data);
            }
        }else{
            chatFragment.addChatLine(data);
        }
    }

    private void postMessage(String data) {
        httpComm.setBasePath("http://52.37.97.113:9000");
        httpComm.setRoute("/api/things");
        httpComm.setRequestMethod(Request.Method.POST);
        httpComm.setJsonBody("{\"name\":\""+mConnection.userName+"\",\"info\":\""+data+"\"}");
        httpComm.jsonRequest();
    }

    @Override
    public void UpdateMove(String move) {
        boardFragment.localTurn = true;
        DisplayToast(move);
        Move mv = ParseMove(move);

        if (chatFragment.connected){
            //Symbol should be assigned to each player
            //possibly assign the server to always be X
            boardFragment.grid.get(0).setSymbol(mv.getSymbol());
            //false parameter is to draw an 'O'
            boardFragment.grid.get(mv.position).setClicked(false);
            boardFragment.CheckForWinner(false);
            boardFragment.CheckForTie(false);
            boardFragment.ChangeGridSymbol();
        }else {
            //Should probably store the move somewhere
        }
    }

    private Move ParseMove(String rawMove) {
        Move move = new Move();
        int first;
        String subStr, pos, symbol;

        if (!rawMove.isEmpty()) {
            first = rawMove.indexOf(":");
            subStr = rawMove.substring(first).substring(1);

            pos = subStr.substring(0, subStr.indexOf(":"));

            subStr = subStr.substring(subStr.indexOf(":")).substring(1);
            symbol = subStr.substring(subStr.indexOf(":")).substring(1);
            Log.d("ParseMove", "symbol:"+symbol);

            move.position = Integer.parseInt(pos);
            move.symbol = symbol;
        }

        return move;
    }

    @Override
    public void onSuccessfulResponse(String res) {
        DisplayToast(res);
        chatFragment.addChatLine(res);
    }

    @Override
    public void onErrorResponse(String error) {
        DisplayToast(error);
    }

    public class Move{
        int position = 0;
        String symbol = "X";

        public boolean getSymbol(){
            Log.d("getSymbol", "symbol:"+symbol);
            Log.d("getSymbol", "bool value:"+(symbol.toLowerCase() == "x"));
            return symbol.toLowerCase() == "x";
        }
    }
}
