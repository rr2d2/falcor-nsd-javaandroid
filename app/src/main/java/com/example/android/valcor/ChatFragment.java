package com.example.android.valcor;

import android.app.Activity;
import android.app.Fragment;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

/**
 * Created by or185014 on 3/7/2016.
 */
public class ChatFragment extends Fragment{
    private final String TAG = "ChatFragment: ";
    NsdHelper mNsdHelper;
    ChatConnection mConnection;
    GameCommunicator gameCommunicator;

    private Activity mActivity;
    private Toast mToast;
    private TextView mStatusView;
    private TextView txtViewStatus;

    boolean connected = false;
    String userName = "UNKNOWN";
    String opponentsName = "UNKNOWN";
    WhoAmI iAm = WhoAmI.UNKNOWN;
    private HttpComm httpComm;

    enum WhoAmI{
        SERVER,
        CLIENT,
        UNKNOWN
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            //blah
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
        mStatusView = (TextView) mActivity.findViewById(R.id.txtViewMessages);
        txtViewStatus = (TextView) mActivity.findViewById(R.id.txtViewStatus);

        setClickEvents();
        mStatusView.setMovementMethod(new ScrollingMovementMethod());
        httpComm = new HttpComm(mActivity.getApplicationContext(), mActivity);

        Handler mUpdateHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                try {
                    if (msg.getData().containsKey("nsd")) {
                        iAm = WhoAmI.SERVER;
                        connected = true;
                        txtViewStatus.setText("SERVER");
                        //Disable connect button
                        Button btnConnect = (Button) mActivity.findViewById(R.id.btnConnect);
                        btnConnect.setEnabled(true);
                        DisplayToast(msg.getData().getString("nsd"));
                        Log.d(TAG, "USER: " + userName + " OPPNAME:" + opponentsName);
                        mConnection.userName = userName;
                        mConnection.opponentsName = opponentsName;
                    } else if (msg.getData().containsKey("con") && iAm != WhoAmI.SERVER) {
                        iAm = WhoAmI.CLIENT;
                        mConnection.sendUserInfo(userName);
                        connected = true;
                        txtViewStatus.setText("CLIENT");

                        WhenPlayersConnected();

                        DisplayToast("You are the client");
                        Log.d(TAG, "USER: " + userName + " OPPNAME:" + opponentsName);
                        mConnection.userName = userName;
                        mConnection.opponentsName = opponentsName;
                    } else if (msg.getData().containsKey("msg")) {
                        String chatLine = msg.getData().getString("msg");
                        addChatLine(chatLine);
                        postMessage(chatLine);
                    } else if (msg.getData().containsKey("opp")){
                        if (opponentsName == "UNKNOWN"){
                            opponentsName = msg.getData().getString("opp");
                            Log.d(TAG, "USER: "+userName+" OPPNAME:"+opponentsName);
                            mConnection.opponentsName = opponentsName;
                            mConnection.sendUserInfo(userName);
                        }
                    }
                    else if (msg.getData().containsKey("mv")){
                        gameCommunicator.UpdateMove(msg.getData().getString("mv"));
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.getStackTrace().toString());
                }
            }
        };

        mConnection = new ChatConnection(mUpdateHandler);
        //May need additional support ...
        mNsdHelper = new NsdHelper(mActivity, mUpdateHandler);
        mNsdHelper.initializeNsd();

    }

    private void postMessage(String message) {
        httpComm.setBasePath("http://52.37.97.113:9000");
        httpComm.setRoute("/api/things");
        httpComm.setRequestMethod(Request.Method.POST);
        httpComm.setJsonBody("{\"name\":\""+getIdentity()+"\",\"info\":\""+message+"\"}");
        httpComm.jsonRequest();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        /*if (mNsdHelper != null) {
            mNsdHelper.stopDiscovery();
        }*/
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (mNsdHelper != null) {
            mNsdHelper.discoverServices();
        }*/
    }

    @Override
    public void onDestroy() {
       /* if (mNsdHelper != null){
            mNsdHelper.tearDown();
        }
        if (mConnection != null){
            mConnection.tearDown();
        }*/
        super.onDestroy();
    }

    public void SetGameCommunicator(GameCommunicator gc){
        gameCommunicator = gc;
    }

    public interface GameCommunicator {
        void UpdateMove(String move);
    }

    private void setClickEvents() {
        Button btnRegister = (Button)mActivity.findViewById(R.id.btnRegister);
        Button btnConnect = (Button)mActivity.findViewById(R.id.btnConnect);
        Button btnClearTxt = (Button)mActivity.findViewById(R.id.btnClearTxt);
        Button btnSend = (Button)mActivity.findViewById(R.id.btnSend);

        btnClearTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickClearText(v);
            }
        });
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickConnect(v);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAdvertise(v);
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSend(v);
            }
        });
    }

    private void WhenPlayersConnected() {
        Button btnSend = (Button) mActivity.findViewById(R.id.btnSend);
        Button btnConnect = (Button) mActivity.findViewById(R.id.btnConnect);
        btnConnect.setEnabled(false);
        btnSend.setEnabled(true);
    }

    public void clickAdvertise(View v) {
        mStatusView.setText("Trying to Register...");
        // Register service
        if(mConnection.getLocalPort() > -1) {
            mNsdHelper.registerService(mConnection.getLocalPort());

            //Disable the registration button
            Button btn = (Button) mActivity.findViewById(R.id.btnRegister);
            btn.setEnabled(false);
        } else {
            Log.d(TAG, "ServerSocket isn't bound.");
        }
    }

    public void clickClearText(View v) {
        mStatusView.setText("");
    }

    public void clickConnect(View v) {
        NsdServiceInfo service = mNsdHelper.getChosenServiceInfo();
        if (service != null) {
            Log.d(TAG, "Connecting.");
            addChatLine("Connecting...");
            DisplayToast("Connecting");

            mConnection.connectToServer(service.getHost(),
                    service.getPort());
            //mConnection.sendUserInfo(userName);
            connected = true;
            WhenPlayersConnected();

        } else if (service == null && mConnection.isChatClientStarted()){
            DisplayToast("You are the client");
            Log.d(TAG, "You are the client!");
            //Disable connect button
            Button btn = (Button) mActivity.findViewById(R.id.btnConnect);
            btn.setEnabled(false);
        } else {
            DisplayToast("No Service to connect to");
            Log.d(TAG, "No service to connect to!");
        }
    }

    public void clickSend(View v) {
        mConnection.sendUserInfo(userName);
        EditText messageView = (EditText) mActivity.findViewById(R.id.chatInput);
        if (messageView != null) {
            String messageString = messageView.getText().toString();
            if (!messageString.isEmpty()) {
                mConnection.sendMessage(messageString);
            }
            messageView.setText("");
        }
    }

    public void addChatLine(String line) {
        mStatusView.append("\n" + getIdentity() +": "+line);
    }

    private String getIdentity() {
        switch (iAm){
            case UNKNOWN:
                return "UNKNOWN";
            case SERVER:
                return "Server";
            case CLIENT:
                return "Client";
            default:
                return "Identity UNKNOWN";
        }
    }

    private void DisplayToast(String msg) {
        if (mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(mActivity.getApplicationContext(), msg, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
