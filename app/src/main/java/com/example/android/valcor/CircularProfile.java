package com.example.android.valcor;

/**
 * Created by js185448 on 2/19/2016.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by js185448 on 2/17/2016.
 */
public class CircularProfile extends ImageView {
    String textToDraw;
    Bitmap imageToShow;
    final String TAG = "CircularProfile: ";

    //Standard constructor Must have to run in android
    public CircularProfile(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setImage(Bitmap image){
        //sets image bitmap to a circularly cropped image
        try {
            imageToShow = circleBitmap(image);
        } catch (IOException e) {
            Log.d(TAG, e.toString());
        }
        invalidate();
    }

    public void setText(String newTxt){
        textToDraw = newTxt;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas){
        //Paint path not a URI/URL path
        if(!(imageToShow == null) && !(textToDraw == null)) {
            Path path = new Path();

            path.addCircle(imageToShow.getWidth() / 2, imageToShow.getHeight() / 2, imageToShow.getHeight() / 2, Path.Direction.CW);

            canvas.drawBitmap(imageToShow, 0, 0, null);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setTextSize(33);
            paint.setTextAlign(Align.CENTER);
            //canvas.drawTextOnPath(textToDraw, path, 0.0f, 0.0f, paint);
            canvas.drawText(textToDraw, 175, 25, paint);
        }
        super.onDraw(canvas);
    }

    public Bitmap circleBitmap(Bitmap bitmap) throws IOException {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;

        return output;
    }


}

