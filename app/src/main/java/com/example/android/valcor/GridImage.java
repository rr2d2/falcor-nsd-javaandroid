package com.example.android.valcor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageButton;


/**
 * Created by js185448 on 2/22/2016.
 */
public class GridImage extends ImageButton {
    Context mContext;
    private final String TAG = "GridImage";
    private static boolean playerSymbol; //false means O and true means X
    private boolean O = false;
    private boolean X = false;
    private boolean clicked = false;
    public GridImage (Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(mContext, android.R.color.holo_green_dark));
        paint.setStrokeWidth(4);

    }
    public GridImage (Context context){
        super(context);
        mContext = context;
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(mContext, android.R.color.holo_green_dark));
        paint.setStrokeWidth(4);

    }
    Paint paint = new Paint(); //X and O paint

    public void setClicked(boolean XorO){
        //Cannot be in callback method because of time lag! will cause winner calculations to be inaccurate
        if(XorO) {
            X = true;
            O = false;
        } else if(!XorO) {
            O = true;
            X = false;
        }

        clicked = true;
        this.setEnabled(false);
        invalidate();
    }

    public void setSymbol(boolean X){
        playerSymbol = X;
        Log.d(TAG, "setting player symbol:"+playerSymbol);
    }

    public boolean getSymbol(){
        Log.d(TAG, "getSymbol:"+playerSymbol);
        return playerSymbol;
    }


    @Override
    public void onDraw(Canvas canvas){
        if(clicked) {
            if(X) {
                drawX(canvas);
            } else if(O) {
                drawO(canvas);
            }
            else {Log.d(TAG, "Symbol not set!"); }
        }
        if(!clicked){
            drawReset(canvas);
        }

        super.onDraw(canvas);
    }

    public boolean isO(){
        return O;
    }

    public boolean isX(){
        return X;
    }

    private void drawO(Canvas canvas){
        Path circlePath = new Path();
        float width = this.getWidth();
        float height = this.getHeight();

        circlePath.addCircle(width/2, height/2,  2*Math.min(width, height)/5, Path.Direction.CCW);
        canvas.drawPath(circlePath, paint);
    }

    private void drawX(Canvas canvas){

        float width = this.getWidth();
        float height = this.getHeight();
        float w_b = width * (float)(1.0/4.0);
        float h_b = height * (float)(1.0/4.0);

        canvas.drawLine(0 + w_b, height - h_b, width - w_b, 0 + h_b, paint);
        canvas.drawLine(0 + w_b, 0 + h_b, width - w_b, height - h_b, paint);
    }

    private void drawReset(Canvas canvas){
        canvas.restore();
    }

    public void resetGridSquare(){
        setSymbol(true);
        X = false;
        O = false;
        clicked = false;
        this.setEnabled(true);
        invalidate();
    }
}
