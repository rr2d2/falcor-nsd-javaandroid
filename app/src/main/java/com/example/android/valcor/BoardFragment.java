package com.example.android.valcor;


import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by js185448 on 2/19/2016.
 */
public class BoardFragment extends Fragment {
    private final String TAG = "BoardFragment: ";
    boolean localTurn = true;
    Communicator comm;
    ArrayList<GridImage> grid = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.board_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedBundleState){
        super.onActivityCreated(savedBundleState);
        //I feel like there is a better way to do this but I couldn't find any
        grid = new ArrayList<GridImage>();

        PopulateGrid(grid);
        /*
        * Grid represented like:
        *
        *        0  1  2
        *        3  4  5
        *        6  7  8
        *
        */
        View.OnClickListener gridBtnHandler = new View.OnClickListener() {
            public void onClick(View v) {
                GridBtnClicked(v, grid);
            }
        };

        //Makes every button in grid call gridBtnHandler when clicked
        //if (userIsHost) ... set symbol to X
        grid.get(0).setSymbol(true); //static and changes it for all clicks

        for (GridImage g : grid) {
            g.setOnClickListener(gridBtnHandler);
        }

        SetupProfiles();
    }

    public void setCommunicator(Communicator c){
        comm = c;
    }

    public interface Communicator{
        //Maybe change parameter to a bundle?
        void respond(String data);
    }

    public void UpdateUserName(String userName){
        CircularProfile userCPV = (CircularProfile) getActivity().findViewById(R.id.UserProfile);
        userCPV.setText(userName);
    }

    public void UpdateOpponentName(String oppName){
        CircularProfile enemyCPV = (CircularProfile) getActivity().findViewById(R.id.EnemyProfile);
        enemyCPV.setText(oppName);
    }

    private void SetupProfiles() {
        CircularProfile userCPV = (CircularProfile) getActivity().findViewById(R.id.UserProfile);
        Bitmap hoddyIcon = BitmapFactory.decodeResource(getResources(), R.drawable.bull);
        //hoddyIcon = Bitmap.createBitmap(hoddyIcon, 0, 0, hoddyIcon.getWidth(), hoddyIcon.getHeight());
        userCPV.setText("NONE");
        userCPV.setImage(hoddyIcon);

        CircularProfile enemyCPV = (CircularProfile) getActivity().findViewById(R.id.EnemyProfile);
        Bitmap falloutIcon = BitmapFactory.decodeResource(getResources(), R.drawable.fallout);
        //falloutIcon = Bitmap.createBitmap(falloutIcon, 0, 0, falloutIcon.getWidth(), falloutIcon.getHeight());
        enemyCPV.setText("Hoddy mic Toddy");
        enemyCPV.setImage(falloutIcon);
    }

    private void PopulateGrid(ArrayList<GridImage> grid) {
        grid.add((GridImage) getActivity().findViewById(R.id.grid00));
        grid.add((GridImage) getActivity().findViewById(R.id.grid10));
        grid.add((GridImage) getActivity().findViewById(R.id.grid20));
        grid.add((GridImage) getActivity().findViewById(R.id.grid01));
        grid.add((GridImage) getActivity().findViewById(R.id.grid11));
        grid.add((GridImage) getActivity().findViewById(R.id.grid21));
        grid.add((GridImage) getActivity().findViewById(R.id.grid02));
        grid.add((GridImage) getActivity().findViewById(R.id.grid12));
        grid.add((GridImage) getActivity().findViewById(R.id.grid22));
    }

    private void GridBtnClicked(View v, ArrayList<GridImage> grid) {
        if (localTurn) {
            localTurn = false;
            int position = -1;
            switch (v.getId()) {
                case R.id.grid00:
                    position = 0;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid10:
                    position = 1;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid20:
                    position = 2;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid01:
                    position = 3;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid11:
                    position = 4;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid21:
                    position = 5;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid02:
                    position = 6;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid12:
                    position = 7;
                    grid.get(position).setClicked(true);
                    break;
                case R.id.grid22:
                    position = 8;
                    grid.get(position).setClicked(true);
                    break;
                default:
                    Log.d(TAG, "Called wrong onClickListnerer");
                    break;
            }

            String symbol = grid.get(0).getSymbol() ? "O" : "X";
            comm.respond("position:" + position + ":symbol:" + symbol);
            //comm.respond("symbol:"+symbol);

            CheckForWinner(true);
            CheckForTie(true);
            //Sets player symbol to X??
            //grid.get(0).setSymbol(true);
            ChangeGridSymbol();
        } else {
            Toast.makeText(getActivity(), "Not your turn", Toast.LENGTH_SHORT).show();
        }
    }

    public void ChangeGridSymbol() {
        grid.get(0).setSymbol(!grid.get(0).getSymbol());
    }

    public void CheckForTie(boolean isLocal) {
        if(tie(grid)){
            comm.respond("Tie");
            Toast.makeText(getActivity(), "Yall TIED!", Toast.LENGTH_SHORT).show();
            resetBoard(grid);
        }
    }

    public void CheckForWinner(boolean isLocal) {
        if(winner(grid)){
            comm.respond("Winner");
            Toast.makeText(getActivity(), (isLocal ? "YOU WIN":"YOU LOST")+"!", Toast.LENGTH_SHORT).show();
            resetBoard(grid);
        }
    }

    public boolean tie(ArrayList<GridImage> grid){
        int OXCount = 0;
        for(GridImage g : grid){
            if(g.isO() || g.isX()){
                OXCount++;
            }
        }

        if(OXCount == 9){
            return true;
        }

        return false;
    }

    public boolean winner(ArrayList<GridImage> grid) {
        boolean playerSymbol = false; //false is O true is X
        boolean[] currSymbols = new boolean[9];

        if(grid.get(0).getSymbol()){
            playerSymbol = true; // X otherwise O
        }

        //making a boolean array from the grid of players symbols
        int counter = 0;
        for(GridImage g : grid){
            if(playerSymbol){
                if(g.isX()){
                    currSymbols[counter] = true;
                }
            } else if(g.isO()){
                currSymbols[counter] = true;
            }
            counter++;
        }

        int[] checks = new int[8]; //these ints all count to three for each row/column/diagonal win possibility,
        // if one of these is 3 then the user wins first 3 are rows, second 3 are columns and last 2 are diagonal wins
        if(currSymbols[0]){checks[0]++; checks[3]++; checks[7]++;}
        if(currSymbols[1]){checks[0]++; checks[4]++;}
        if(currSymbols[2]){checks[5]++; checks[0]++; checks[6]++;}
        if(currSymbols[3]){checks[1]++; checks[3]++; }
        if(currSymbols[4]){checks[4]++; checks[7]++; checks[6]++; checks[1]++;}
        if(currSymbols[5]){checks[5]++; checks[1]++; }
        if(currSymbols[6]){checks[2]++; checks[3]++; checks[6]++;}
        if(currSymbols[7] ){checks[4]++; checks[2]++; }
        if(currSymbols[8]){checks[2]++; checks[5]++; checks[7]++;}
        for(int i : checks){
            if(i == 3){

                return true;
            }
        }
        return false;
    }

    public void resetBoard(ArrayList<GridImage> grid){
        for(GridImage g : grid){
            g.resetGridSquare();
        }
    }

}